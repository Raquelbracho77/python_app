#!/usr/bin/env python3

from html.parser import HTMLParser

'''
Clase que permite buscar las etiquetas o tags de un archivo
html
'''

class Titulo(HTMLParser):
	
	''' --------------------------------
		          METODOS 
	---------------------------------'''
	
	'''
	metodo __init__ , se utiliza para ...
	'''
	def __init__(self, n = 1, bloque = "body", **kwargs):
		# ~ tags, utilizamos esta lista comprehensiva para buscar
		# ~ las etiquetas en el archivo html con un condicional
		# ~ donde las etiquetas deben ser menores a h6
		self.tags = [f"h{i+1}" for i in range(n) if i < 6] 
		# titulos como diccionario 
		self._titulos = {}
		# contenidos como diccionario
		self._contenidos = {}
		# Bandera o aviso para indicar que el elemento entra a la variable 'in_bloque' en valor booleano false
		self.in_bloque = False
		# instancia bloque asignandole el valor que ocupa en el parametro dentro del metodo
		self.bloque = bloque
		# instancia contador a 0
		self.contador = 0
		# Bandera o aviso para indicar que el elemento entra a la variable 'add_data' en valor booleano false
		self.add_data = False
		self.is_class = False
		self.add_content = False
		# instancia clases asignandole un diccionario como 'clases' para luego recuperarlo en el metodo handle_starttags (line:70)
		self.clases = kwargs.get("clases", [])
		# super inicializa los parametros heredados de la clase titulo
		# ~ De la clase superior que es HTMLParser super le hereda los atributos al metodo init para no tener que volver a llamarlos, 
		super().__init__()
		
		
		'''Metodo reset, se encarga de 'limpiar' el contenido de las variables
		en memoria llegado el momento de imprimir los titulos'''
	def reset(self):
		# ~ De la clase superior que es HTMLParser super le hereda los atributos al metodo reset para no tener que volver a llamarlos,
		super().reset()
		# titulos como diccionario
		self._titulos = {}
		# instancia bloque asignandole el valor que ocupa en el parametro dentro del metodo
		self.in_bloque = False
		self.contador = 0
		print("Se ha eliminado el contenido de las variables.")
		
		
	# ~ Este método se ocupa únicamente de las etiquetas de inicio, 
	# ~ como  <title> . El argumento de la etiqueta se refiere al 
	# ~ nombre de la etiqueta de inicio, mientras que los atributos 
	# ~ se refieren al contenido dentro de la etiqueta de inicio.
	def handle_starttag(self, etiqueta, attrs):
		# si el parametro 'etiqueta' es igual a la etiqueta dentro del bloque
		if etiqueta == self.bloque:
			#entra dentro del bloque
			self.in_bloque = True
			# suma la etiqueta
			self.contador += 1
			# muestra el nombre de la etiqueta y hasta el momento cuantas lleva
			self.llave = f"{etiqueta}_{self.contador}"
			# ocupa un diccionario para los titulos a recopilar, _titulos es un dict
			self._titulos[self.llave] = []
			# muestra la etiqueta inicial
			print("Etiqueta o tag inicial:", etiqueta)
			# compara si la etiqueta que está en 'tags' y dentro del bloque 'in_bloque', entonces:
		if etiqueta in self.tags and self.in_bloque:
			# si la etiqueta está, la agrega a los datos 
			self.add_data = True
		for name,values in attrs:
			if name == "class":
				valores = list(filter(lambda e: e in self.clases,values.split()))
				if valores:
					self.add_data = True
					self.is_class = True
					self.add_content = True
		if etiqueta == 'body':
			self.add_content = True
	
	# Identifica las etiquetas finales del bloque
	def handle_endtag(self, etiqueta):
		if etiqueta == self.bloque:
			self.in_bloque = False
			print("Etiqueta o tag final:", etiqueta)
		if etiqueta in self.tags and self.in_bloque:
			self.add_data = False
		if etiqueta == 'body':
			self.add_content = False


# ~ esta es la función donde se realiza un proceso para extraer 
# ~ valores de las etiquetas HTML y se pasan los datos relacionados con cada etiqueta
	def handle_data(self, data):
		if self.add_data:
			self._titulos[self.llave].append(data)
			if self.is_class:
				self.add_data = False
				self.is_class = False
		if self.add_content:
			self._contenidos[self.llave] = data
			print("Contenido del 'body':", data)


	@property
	def titulos(self):
		return self._titulos
		
		
	@property
	def contenidos(self):
		return self._contenidos

