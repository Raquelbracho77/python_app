#!/bin/usr/python3
from sys import argv
from os.path import exists
import gzip

def descomprimir(nombre_archivo):
	archivo = gzip.open(nombre_archivo)
	write_archivo(nombre_archivo[:nombre_archivo.rfind(".gz")], archivo.read())
	f.close()


def main():
	argc = len(argv)
	
	if argc > 1:
		for nombre_archivo in argv[1:]:
			if exists(nombre_archivo):
				if nombre_archivo.lower.endswith("."):
					descomprimir(nombre_archivo)
				else:
					print("No hay nada")
			else:
				print("No se ha encontrado %s" %nombre_archivo)
	else:
		print(u"No se ha encontrado ningun archivo")
		

if __name__ == "__main__":
	main()
